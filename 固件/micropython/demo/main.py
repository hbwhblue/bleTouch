#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2023-9-03 0:06:58
# @Author  : mage
# @Link    : http://fengmm521.taobao.com
import touchTool
import time
import uartUtil

#宝箱
def baoxiang():
    print("baoxiang")

# 红包雨
def hongbaoyu():
    print("hongbaoyu")
    
def main(): 
    print("start")
    uartUtil.start()
    touchTool.set_screenWH(1080,1920)
    touchTool.start()
    time.sleep(1)     #等3秒,手动配对成功,这个时间根据不同手机决定
    print("start and connect")
    while True:
        dat = uartUtil.reciveDat()
        if dat:
            try:
                dat = dat.decode()
                # print("recive data",dat)
                ds = dat.split(",")
                tp = int(ds[0])
                x = int(ds[1])
                y = int(ds[2])
                if tp == 0:#移动 
                    touchTool.moveTo(x,y)
                elif tp == 1:#按下
                    touchTool.moveTo(x,y,True)
                elif tp == 2:#松开
                    touchTool.moveTo(x,y,False)
                elif tp == 3:#点击一下坐标x,y
                    touchTool.click()
                elif tp == 4:#拖动或者叫滑动,ds1,ds2为起始坐标,ds3,ds4为结束坐标,ds5为滑动时间毫秒数
                    touchTool.slide(ds[1],ds[2],ds[3],ds[4],ds[5])
                    pass
                elif tp == 10:#设置板子上电并且蓝牙连接后延时执行函数，x为延时时间，y为执行函数编号
                    pass
                elif tp == 100:#设置手机屏坐标分辨率
                    touchTool.set_screenWH(int(x),int(y))
                elif tp == 101:#开始录制点击相关动作
                    pass
                elif tp == 102:#结束录制点击相关动作,x为保存脚本编号
                    pass
                elif tp == 103:#删除录制脚本,x为脚本编号
                    pass
                elif tp == 104:#删除所有脚本
                    pass
                elif tp == 105:#下载录制脚本,x为脚本编号
                    pass
                elif tp == 106:#上传脚本，x为脚本编号,y为上传的脚本字节数
                    pass
                elif tp == 200:
                    #执行脚本编号x的脚本录制点击数据,y为执行脚本次数（0为无限次），ds[3]为点击坐标随机偏移x方向范围，
                    #ds[4]为点击坐标随机偏移y方向范围,ds[5]为点击间隔时间随机延时范围从0到ds[5]
                    pass
                elif tp == 201:#停止运行中的脚本
                    pass
                else:
                    print('data error')
                print("ok")
            except:
                print("data error")
                time.sleep_ms(2)
                continue
        else:
            time.sleep_ms(2)
            
if __name__ == '__main__':
    main()