# Ble蓝牙键盘鼠标和wifi版16头点击器

## 介绍
新款的带有蓝牙5.2可以模拟蓝牙OTG鼠标和蓝牙键盘的点击器,是之前的16头wifi版击器加强版,本产品使用ESP32-WROOM-32d模块,是上一代wifi版点击器的加强版,除了支持上一代wifi版点击器的所有功能外,还支持模拟蓝牙键盘和蓝牙鼠标,并且键盘和鼠标为免驱动支持手机安卓,iOS和电脑的windows,linux,Mac os系统.

## 如何获取硬件

硬件唯一购买地址:
https://item.taobao.com/item.htm?id=722805535129

## 功能介绍

##### 一.基础功能:

1.支持PC端通过wifi控制16个外部物理点击头点击
2.支持PC端通过USB的串口控制外部物理点击头点击
3.支持micropython程序直接在主控板上脱机运行控制16个外部物理点击头点击

以上三种控制方式都和上一代产品一样提供源码

##### 二.新增功能:

新增通过串口控制物理点击头点击电容屏的同时,还支持通过串口模拟蓝牙键盘和蓝牙鼠标进行按键输入和鼠标点击

因为主控芯片原因,主控板无法同时支持蓝牙和wifi,所以当使用蓝牙鼠标和蓝牙键盘功能只能过通过电脑USB串口控制模拟鼠标和键盘动作

micropython版固件的蓝牙键盘蓝牙鼠标目前还在开发中,故暂时不支持脱机使用蓝牙键盘和蓝牙鼠标功能


#### 安装教程

上位机程序使用python开发,所以要使用本点击器需要上位机电脑安装python才能运行上位机的示例程序.建议安装python3.8及以上版本python.我们所提供的python示例程序都是在python3.8版本下开发,理论上python3.8的版本也支持,但更高版本的python我们没有对基进行过测试,有问题可以联系我

安装好python后,还需要安装python的串口库,安装好python后可以使用pip进行安装
``` bash
pip install pyserial -i https://pypi.tuna.tsinghua.edu.cn/simple

```

如果要使用令命行烧写esp32和esp8266固件还需要安装esptool的python工具:
``` bash
pip install esptool -i https://pypi.tuna.tsinghua.edu.cn/simple

```

python和其他调试工具,以及代码编辑器vscode获取网盘地址:

链接: https://pan.baidu.com/s/1Izdy5mgndu-9RsO-Y46McA?pwd=1234 提取码: 1234

USB转串口USB驱动(mac os 10.14及以上版本系统自代驱动,win7和win10系统可能需要安装USB驱动):

链接: https://pan.baidu.com/s/120bRP7EKF-oraQcdhxzfbQ?pwd=1234 提取码: 1234 


#### 使用说明

这里只说明一下USB串口发送指令控制鼠标,键盘和物理点击头的方法,本蓝牙wifi板16头点击器默认发货烧写这个固件:

##### 蓝牙绝对鼠标串口控制指令

```python
[x,y,1]
```
上边的指令是在[ ]中间的字符串分为3个部分,下边进行说明
 **x**  | **y** | **1** 
---------|----------|----------
鼠标x坐标    | 鼠标y坐标    | 点击状态   
 0~25400  | 0~15875  | 0,1,2,3  

坐标值说明:
x坐标在最左边的值为0,最右边的值为25400,坐标和手机实际物理坐标是线性关系,即x坐标在屏中间时值为(25400/2)
y坐标同x坐标类似,即y坐标在最上边时值为0,在最下边是值为15875,实际物理坐标和这个值是线性关系

后边的点击状态说明:
0:抬起状态
1:左键按下
2:右键按下
3:左键点击一下

使用串口发送指令时要包含[]方括号,中间不能有空格.例如:

```pythoh
[5000,5000,1]
```
表示鼠标在5000,5000这个坐标按下

##### 1.鼠标在(0,0)按下
发送在(0,0)坐标下鼠标按下,按下力度511,指令如下
```python
[0,0,1]
```

##### 2.鼠标在(0.0)抬起
```python
[0,0,0]
```

##### 3.鼠标在(0,0)坐标点击一下

```python
[0,0,3]
```

##### 4.鼠标右键按下
```python
[0,0,2]
```
右键按下只在电脑上起作用,手机上还是相当于左键

### 鼠标的滑动操作

鼠标滑动有两个方式来控制,
1)使用上边的按下指令发送一整个滑动轨迹即可
2)使用下边单独的滑动指令

```python
{x1,y1,x2,y2,speed}
```
这个指令说明,这是一个按下滑动,或者叫托动也可以:
x1,y1:为滑动起点坐标
x2,y2:为滑动终点坐标
speed:为滑动的速度,单位是每秒移动值

注意串口发送时两边的 {} 花括号不能省.

##### 蓝牙键盘控制指令
表示一个按键按下
```python
#键盘字符串或者单个字符的ASCII码+按键状态
(+键盘按键的16进制字节码)
```
表示一个按键抬起
```
(-键盘按键的16进制字节码)
```
表示一个按键被点击一次
```
(~键盘按键的16进制字节码)
```
媒体按键按下
```
(@后跟两个字节的媒体字节码)
```
媒体按键抬起
```
(^后跟两个字节的媒体字节码)
```

所以字节码都为16进制的字符串表示,()圆括号也要在串口中发送,括号是区分指令是鼠标还是键盘按键的标志,所以发送时不能少

下边是电脑键盘和媒体按键值的说明

``` Java
//键盘动作

// const uint8_t KEY_LEFT_CTRL = 0x80;
// const uint8_t KEY_LEFT_SHIFT = 0x81;
// const uint8_t KEY_LEFT_ALT = 0x82;
// const uint8_t KEY_LEFT_GUI = 0x83;
// const uint8_t KEY_RIGHT_CTRL = 0x84;
// const uint8_t KEY_RIGHT_SHIFT = 0x85;
// const uint8_t KEY_RIGHT_ALT = 0x86;
// const uint8_t KEY_RIGHT_GUI = 0x87;

// const uint8_t KEY_UP_ARROW = 0xDA;
// const uint8_t KEY_DOWN_ARROW = 0xD9;
// const uint8_t KEY_LEFT_ARROW = 0xD8;
// const uint8_t KEY_RIGHT_ARROW = 0xD7;
// const uint8_t KEY_BACKSPACE = 0xB2;
// const uint8_t KEY_TAB = 0xB3;
// const uint8_t KEY_RETURN = 0xB0;
// const uint8_t KEY_ESC = 0xB1;
// const uint8_t KEY_INSERT = 0xD1;
// const uint8_t KEY_DELETE = 0xD4;
// const uint8_t KEY_PAGE_UP = 0xD3;
// const uint8_t KEY_PAGE_DOWN = 0xD6;
// const uint8_t KEY_HOME = 0xD2;
// const uint8_t KEY_END = 0xD5;
// const uint8_t KEY_CAPS_LOCK = 0xC1;
// const uint8_t KEY_F1 = 0xC2;
// const uint8_t KEY_F2 = 0xC3;
// const uint8_t KEY_F3 = 0xC4;
// const uint8_t KEY_F4 = 0xC5;
// const uint8_t KEY_F5 = 0xC6;
// const uint8_t KEY_F6 = 0xC7;
// const uint8_t KEY_F7 = 0xC8;
// const uint8_t KEY_F8 = 0xC9;
// const uint8_t KEY_F9 = 0xCA;
// const uint8_t KEY_F10 = 0xCB;
// const uint8_t KEY_F11 = 0xCC;
// const uint8_t KEY_F12 = 0xCD;
// const uint8_t KEY_F13 = 0xF0;
// const uint8_t KEY_F14 = 0xF1;
// const uint8_t KEY_F15 = 0xF2;
// const uint8_t KEY_F16 = 0xF3;
// const uint8_t KEY_F17 = 0xF4;
// const uint8_t KEY_F18 = 0xF5;
// const uint8_t KEY_F19 = 0xF6;
// const uint8_t KEY_F20 = 0xF7;
// const uint8_t KEY_F21 = 0xF8;
// const uint8_t KEY_F22 = 0xF9;
// const uint8_t KEY_F23 = 0xFA;
// const uint8_t KEY_F24 = 0xFB;

// typedef uint8_t MediaKeyReport[2];

// const MediaKeyReport KEY_MEDIA_NEXT_TRACK = {1, 0};
// const MediaKeyReport KEY_MEDIA_PREVIOUS_TRACK = {2, 0};
// const MediaKeyReport KEY_MEDIA_STOP = {4, 0};
// const MediaKeyReport KEY_MEDIA_PLAY_PAUSE = {8, 0};
// const MediaKeyReport KEY_MEDIA_MUTE = {16, 0};
// const MediaKeyReport KEY_MEDIA_VOLUME_UP = {32, 0};
// const MediaKeyReport KEY_MEDIA_VOLUME_DOWN = {64, 0};
// const MediaKeyReport KEY_MEDIA_WWW_HOME = {128, 0};
// const MediaKeyReport KEY_MEDIA_LOCAL_MACHINE_BROWSER = {0, 1}; // Opens "My Computer" on Windows
// const MediaKeyReport KEY_MEDIA_CALCULATOR = {0, 2};
// const MediaKeyReport KEY_MEDIA_WWW_BOOKMARKS = {0, 4};
// const MediaKeyReport KEY_MEDIA_WWW_SEARCH = {0, 8};
// const MediaKeyReport KEY_MEDIA_WWW_STOP = {0, 16};
// const MediaKeyReport KEY_MEDIA_WWW_BACK = {0, 32};
// const MediaKeyReport KEY_MEDIA_CONSUMER_CONTROL_CONFIGURATION = {0, 64}; // Media Selection
// const MediaKeyReport KEY_MEDIA_EMAIL_READER = {0, 128};

```



##### 物物16个点击头控制指令

这部分控制指令和之前上一个版本的wifi点击器一样,使用两个字节16位控制16个点击头的状态,16位从右向左每一个二进制位代表一个点击头状态,0为按下1为抬起,例如:

16个点击头都抬起不按的指令:
```
<FFFF>
```
16个点击头同进都按下的指令:
```
<0000>
```
J1点击头按下:
```
<FFFE>
```
J2点击头按下:
```
<FFFD>
```
J16按下
```
<7FFF>
```
J1和J16同时按下:
```
<7FFE>
```
以下为各个点击头单独按下时的指令值的16进制和二进制表示:
 **点击头编号** | **16进制表示** | **二进制表示**           
-----------|------------|---------------------
 J1        | FFFE       | 1111 1111 1111 1110 
 J2        | FFFD       | 1111 1111 1111 1101 
 J3        | FFFB       | 1111 1111 1111 1011 
 J4        | FFF7       | 1111 1111 1111 0111 
 J5        | FFEF       | 1111 1111 1110 1111 
 J6        | FFDF       | 1111 1111 1101 1111 
 J7        | FFBF       | 1111 1111 1011 1111 
 J8        | FF7F       | 1111 1111 0111 1111 
 J9        | FEFF       | 1111 1110 1111 1111 
 J10       | FDFF       | 1111 1101 1111 1111 
 J11       | FBFF       | 1111 1011 1111 1111 
 J12       | F7FF       | 1111 0111 1111 1111 
 J13       | EFFF       | 1110 1111 1111 1111 
 J14       | DFFF       | 1101 1111 1111 1111 
 J15       | BFFF       | 1011 1111 1111 1111 
 J16       | 7FFF       | 0111 1111 1111 1111 

### 其他命令

串口发送:&

会返回设备mac地址

下边的两个功能目前只在相对坐标固件下完成开发,绝对坐标暂时不支持下边两个命令,后期也会在绝对坐标固件中增加,增加后会自行修改当前markdown文件说明.

串口发送:*

会返回蓝牙名称和一个编号

串口发送:{英文蓝牙名,一个编号}

可以设置蓝牙名,如果手机之前连过个这个蓝牙,蓝牙名称要在手机再次连接成功后才会在手机正确显示新的名称

### 使用示例程序说明

上位机有一个python写的示例程序.主要功能是先控制所有16个物理点击头点击,再控制鼠标到指令点,再控制鼠标在中间从上到下滑动一次.大家要开发功能可以参考


### 蓝牙相对鼠标指令说明

##### 指令格式:

[鼠标按键状态字节,x轴移动最大像素值，x轴移动最大步数，y轴移动最大像素值，y轴移动最大步数，滚轮上下移动最大值，滚轮移动最大步数，滚轮左右移动最大值，滚轮移动最大步数]

//每次接收方括号中9个参数，当没有相应控制时值可以设置为0


一个指令分为五个部分,
第一个部分:

	鼠标按键状态控制
	占一个字节
	鼠标按键一共有5个,左,中,右,向前,向后,对应的字节码分别是:
	//0x01,0x02,0x04,0x08,0x10

第二部分:
	
    鼠标x方向移动控制
    占两个字节
    第一个字节,鼠标移动最大像素值:表示鼠标蓝牙发送移动值的最大值
    第二个字节,鼠标最大值移动步数:表不鼠标移动值从0增加到最大值后保持最大值的发送次数
    
对于移动控制的理解,可以网上搜"相对坐标鼠标移动算法"来理解手机和电脑的相对坐标移动算法是如何计算移动速度和坐标的.这里简单解释一下:

``` Java
//相对坐标的鼠标移动算法相当复杂,微软和苹果都没有开源鼠标移动相关加速和坐标计算算法.只知道目前鼠标的移动是在固定间隔时间向手机或者电脑发送一个加速值,这个值范围在-127到127之前,即一个有符号字节数.我的点击器为了每次发送的数据都产生相同的结果,把计算时间间隔放在了控制板里,所以这里的控制是两个字节.当发送控制指令后,板子会解析出最大移动值,然后在固定8ms的时间里,发关的值从1每次加1直到这个最大值,然后再固定每8ms发送1次最大值,一共发送最大值移动步数次.再慢慢每8ms减1,直到减小到0然后鼠标停下
```
    
第三部分:
	
    鼠标y方向移动控制
    占两个字节
    第一个字节,鼠标移动最大像素值:表示鼠标蓝牙发送移动值的最大值
    第二个字节,鼠标最大值移动步数:表不鼠标移动值从0增加到最大值后保持最大值的发送次数
	
第四部分:
	
    鼠标垂直滚轮移动控制
    占两个字节
    第一个字节,鼠标滚轮最大值:表示鼠标蓝牙发送滚轮值的最大值
    第二个字节,鼠标最大值滚轮步数:表不鼠标滚轮值从0增加到最大值后保持最大值的发送次数
    
第五部分:

	鼠标水平滚轮移动控制(水平方向的滚轮ios好像不支持,所以这两个字节默认都是0x00就好)
    占两个字节
    第一个字节,鼠标滚轮最大值:表示鼠标蓝牙发送滚轮值的最大值
    第二个字节,鼠标最大值滚轮步数:表不鼠标滚轮值从0增加到最大值后保持最大值的发送次数
    
例如:
鼠标向右移动一个距离
```
[000a09000000000000]
```
鼠标向左移动一个距离
```
[00f609000000000000]
```
鼠标按下
```
[010000000000000000]
```
鼠标抬起
```
[000000000000000000]
```
鼠标向右滑动
```
[010a09000000000000]
```
鼠标滚轮向下
```
[00000000000a090000]
```

### 相对坐标实时控制接口

上边的相对坐标是使用主控板里的先加速再减速的方法,下边这里说的是实时控制发送数据间隔的方法

指令如下
```
[0000000000]
```
一共是5个字节的字符表示,

	第一个字节:鼠标按键状态
	第二个字节:鼠标相对坐标中的x下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数
	第三个字节:鼠标相对坐标中的y下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数
	第四个字节:鼠标相对坐标中的鼠标滚轮下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数
    第五个字节:鼠标相对坐标中的鼠标左右方向滚轮下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数

有问题可以加微信:
woodmage


