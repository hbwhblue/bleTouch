
import os
import json
import cmdUtil
import serialUtil
from queue import Queue
import time
import cv2


# IMGSIZE = (1080,1920)
IMGSIZE = (1080,2160) #红米

CMDQUEUE = Queue()

os.getcwd()

def onImgSizeChange(s):
    global IMGSIZE
    IMGSIZE = s
    cmdUtil.initCmdUtil(CMDQUEUE,IMGSIZE)

#完成扫码返回主界面
backMenus = None

#输入价格界面中的数字键盘
numdic = None

#从主界面到设置价格界面
toNums = None

def initAllPoint():
    global backMenus,numdic,toNums
    f = open('backMenu.txt','r')
    bmstr = f.read()
    f.close()
    backMenus = []
    lines = bmstr.replace('\r','').split('\n')
    for i,v in enumerate(lines):
        tmps = v.split(':')
        tmp = [tmps[0],float(tmps[1])]
        backMenus.append(tmp)

    f = open('toSetNum.txt','r')
    bmstr = f.read()
    f.close()
    toNums = []
    lines = bmstr.replace('\r','').split('\n')
    for i,v in enumerate(lines):
        tmps = v.split(':')
        tmp = [tmps[0],float(tmps[1])]
        toNums.append(tmp)

    f = open('num.txt','r')
    bmstr = f.read()
    f.close()
    numdic = json.loads(bmstr)

#前往设置收款价格界面
def toSetNumUI():
    for i,v in enumerate(toNums):
        time.sleep(v[1])
        cmdUtil.sendCmd(v[0])

#返回微信主界面
def backMenu():
    for i,v in enumerate(backMenus):
        time.sleep(v[1])
        cmdUtil.sendCmd(v[0])
        

#设置价格并显示二维码
def setNum(price):
    pstr = '%.2f'%(price)
    for i,v in enumerate(pstr):
        if v != '.':
            pt = numdic[v]
            cmdUtil.sendCmd('[%d,%d,1]'%(pt[0],pt[1]))
            time.sleep(0.05)
            cmdUtil.sendCmd('[%d,%d,0]'%(pt[0],pt[1]))
            time.sleep(0.1)
        elif v == '.':
            pt = numdic['p']
            cmdUtil.sendCmd('[%d,%d,1]'%(pt[0],pt[1]))
            time.sleep(0.05)
            cmdUtil.sendCmd('[%d,%d,0]'%(pt[0],pt[1]))
            time.sleep(0.1)
    time.sleep(0.3)
    
def setOneNum(n):
    if n != '.':
        pt = numdic[n]
        cmdUtil.sendCmd('[%d,%d,1]'%(pt[0],pt[1]))
        time.sleep(0.05)
        cmdUtil.sendCmd('[%d,%d,0]'%(pt[0],pt[1]))
        time.sleep(0.1)
    elif n == '.':
        pt = numdic['p']
        cmdUtil.sendCmd('[%d,%d,1]'%(pt[0],pt[1]))
        time.sleep(0.05)
        cmdUtil.sendCmd('[%d,%d,0]'%(pt[0],pt[1]))
        time.sleep(0.1)
    elif n == 'b':
        pt = numdic['back']
        cmdUtil.sendCmd('[%d,%d,1]'%(pt[0],pt[1]))
        time.sleep(0.05)
        cmdUtil.sendCmd('[%d,%d,0]'%(pt[0],pt[1]))
        time.sleep(0.1)

def setNumOK():
    pt = numdic['ok']
    cmdUtil.sendCmd('[%d,%d,1]'%(pt[0],pt[1]))
    time.sleep(0.05)
    cmdUtil.sendCmd('[%d,%d,0]'%(pt[0],pt[1]))
    time.sleep(0.1)

#接收到已付款音频,并核对付款金额
def onPay(pay):
    pass

#当前手机显示状态
#0:主界面
#1:设置价格界面
#2:设置好价格,显示二维码界面
#3:得到已付款或者付款超时,返回到主界面
nowState = 0

#当前设置的价格
price = 0.0     

def onKey_Event(key):
    global nowState
    print(key)
    if key == 32 or key == 13:  #空格:32,#回车:13
        if nowState == 0:
            toSetNumUI()
            nowState = 1        #切换到输入价格界面
        elif nowState == 1:     #输入确定,并显示二维码
            setNumOK()
            nowState = 2
        elif nowState == 2:     #完成支持或者超时,返回主界面
            backMenu()
            nowState = 0
    if key == 27:    #ESC按键,退出程序
        exit(0)      
    if nowState == 1:
        if key == 48: #数字0
            setOneNum('0')
        elif key == 49: #数字1
            setOneNum('1')
        elif key == 50: #数字2
            setOneNum('2')
        elif key == 51: #数字3
            setOneNum('3')
        elif key == 52: #数字4
            setOneNum('4')
        elif key == 53: #数字5
            setOneNum('5')
        elif key == 54: #数字6
            setOneNum('6')
        elif key == 55: #数字7
            setOneNum('7')
        elif key == 56: #数字8
            setOneNum('8')
        elif key == 57: #数字9
            setOneNum('9')
        elif key == 46: #数字9
            setOneNum('.')
        elif key == 8: #回退键
            setOneNum('b')

def touchInit():
    cmdUtil.release(0,0)
    time.sleep(0.1)
    cmdUtil.release(540,960)
    time.sleep(0.1)
    cmdUtil.release(1080,1920)
    time.sleep(0.1)
    cmdUtil.release(540,960)
    time.sleep(0.1)
    cmdUtil.release(0,0)

def main():
    net_thread = serialUtil.serialThread(CMDQUEUE)
    net_thread.setDaemon(True)
    net_thread.start()
    initAllPoint()
    time.sleep(3)
    cmdUtil.initCmdUtil(CMDQUEUE,IMGSIZE)
    # touchInit()
    time.sleep(0.12)
    img = cv2.imread('m.jpg')
    while True:
        cv2.imshow('fengmm521.taobao.com', img)
        key = cv2.waitKey(0)
        if key > 0:
            onKey_Event(key)

if __name__ == '__main__':  
    main()
