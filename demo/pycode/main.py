#!/usr/bin/env python
# -*- coding: utf-8 -*-
#本代码来自所出售产品的淘宝店店主编写
#未经受权不得复制转发
#淘宝店：https://fengmm521.taobao.com/
#再次感谢你购买本店产品
from ast import Try
import os,sys
import serial
import time
import json
import math

import random

try:
    import ch340usb
except Exception as e:
    pass

from sys import version_info  

isTest = False


MAX_X = 25400   #最大逻辑x坐标
MAX_Y = 15875   #最大逻辑y坐标

MAX_T = 8191    #最大按压力度


W = 1440       #定义手机屏宽度
H = 3200         #定义手机屏高度

wscal = float(MAX_X)/float(W)
hscal = float(MAX_Y)/float(H)

LEFTDOWN = '21'     #左键按下
RIGHTDOWN = '23'    #右键按下
ALLUP     = '20'    #按键抬起

REFLASHTIME = 8     #鼠标刷新时间,单位:ms


def randomTime(pmin,pmax):
    d = random.randint(pmin,pmax)
    time.sleep(d/1000.0)
def conventPoint(p):
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    return (x,y)

def leftDown(px,py):
    p = (px,py)
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,1]'%(x,y)
    return out

def rightDown(px,py):
    p = (px,py)
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,2]'%(x,y)
    return out

def click(px,py):
    p = (px,py)
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,3]'%(x,y)
    return out

def allUp(px,py):
    p = (px,py)
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,0]'%(x,y)
    return out
# 已知平面上有两个不复合的点p1(x1,y1),P2(x2,y2).现在要求物体从p1以速度v移动到p2,用python写一个函数,输入时间t,计算并返回当前物体坐标p(x,y)
#从p1滑动到p2,speed:每秒移动的像素

def move(p1,p2,sp = 20000):
    p1tmp = conventPoint(p1)
    p2tmp = conventPoint(p2)
    ostr = '{%d,%d,%d,%d,%d}'%(p1tmp[0],p1tmp[1],p2tmp[0],p2tmp[1],sp)
    return ostr

serialDelay = 0.02

#获取当前python版本
def pythonVersion():
    return version_info.major


def readcom(t):
    n = t.inWaiting()
    while n<=0:
        time.sleep(serialDelay)
        n = t.inWaiting()
    pstr = t.read(n)
    if pythonVersion() > 2:
        print(pstr.decode())
    else:
        print(pstr)
    

def sendcmd(t,cmd):
    sendstr = cmd
    # if cmd[-1] != '\r':
    #     sendstr += '\r'
    print(sendstr)
    if pythonVersion() > 2:
        s = t.write(sendstr.encode())
    else:
        s = t.write(sendstr.encode())
    t.flush()

def sendAndread(t,v):
    if isTest:
        f = open('test.txt','a')
        f.write(v + '\n')
        f.close()
    else:
        sendcmd(t,v)
        time.sleep(serialDelay)
        readcom(t)


def getSerialConfig():
    f = open('config.txt','r')
    dat = f.read()
    f.close()
    tmpdict = json.loads(dat)
    return tmpdict


def runcom():
    
    conf = getSerialConfig()
    dev = conf['port']
    btv = conf['btv']
    t = serial.Serial(dev,btv,timeout=1)
    if t:
        print(t.name)               #串口名
        print(t.port)               #串口号
        print(t.baudrate)           #波特率
        print(t.bytesize)           #字节大小
        print(t.parity)             #校验位N－无校验，E－偶校验，O－奇校验
        print(t.stopbits)           #停止位
        print(t.timeout)            #读超时设置
        print(t.writeTimeout)       #写超时
        print(t.xonxoff)            #软件流控
        print(t.rtscts)             #硬件流控
        print(t.dsrdtr)             #硬件流控
        print(t.interCharTimeout)   #字符间隔超时
        print('-'*10)
        time.sleep(1)

        for i in range(2):
            print(2-i)
            time.sleep(1)
        for tmp in range(1):
            sendAndread(t,'<0000>')
            time.sleep(0.3)
            sendAndread(t,'<FFFF>')
            time.sleep(0.3)

        ostr = allUp(W/3.0,H*9/10)     
        print(ostr)
        sendAndread(t,ostr)
        for i in range(50):
            time.sleep(0.1)
            ostr = move((W/3.0,H*8/10),(W/3.0,H*3/10))     #生成 从下向上滑动 指令
            sendAndread(t,ostr)                            #向点击器发送指令
            randomTime(2000,2500)                          #随机延时时间，单位ms
            ostr = click(W/2.0,H*2/5)                      #点击一次
            sendAndread(t,ostr)
            randomTime(2000,2500)                            #延时时间，单位s
            ostr = click(W/2.0,H*11/20)               #点击一次
            sendAndread(t,ostr)
            randomTime(31000,32000)                             #延时时间，单位s
            ostr = click(1280,180)                   #点击一次
            sendAndread(t,ostr)
            randomTime(1500,2000)


        time.sleep(1)
        t.close()
    else:
        print('串口不存在')

def main():
    runcom()
def test():
    move((0,0),(800,800))
if __name__ == '__main__':
    main()
    # test()