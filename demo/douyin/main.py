
import os
import json
import random
import cmdUtil
import serialUtil
from queue import Queue
import time
import cv2


# IMGSIZE = (1080,2340) #小米10
# IMGSIZE = (1080,1920)    #小米IMax
IMGSIZE = (768,1024)  #小米

CMDQUEUE = Queue()

os.getcwd()

def onImgSizeChange(s):
    global IMGSIZE
    IMGSIZE = s
    cmdUtil.initCmdUtil(CMDQUEUE,IMGSIZE)


def touchMoveUP():
    spxmin = int(768*0.2)
    spxmax = int(768*0.8)
    
    epymin = int(1024*0.2)
    epymax = int(1024*0.3)
    
    spymin = int(1024*0.7)
    spymax = int(1024*0.8)

    randsx = random.randint(spxmin,spxmax)
    randsy = random.randint(spymin,spymax)

    randex = random.randint(spxmin,spxmax)
    randey = random.randint(epymin,epymax)
    
    cmdUtil.touchMoveTo((randsx,randsy),(randex,randey),20000)


def onKey_Event(key):
    global nowState
    print(key)
    if key == 32 or key == 13:  #空格:32,#回车:13
        touchMoveUP()
    if key == 27:    #ESC按键,退出程序
        exit(0)      
    if key == 48: #数字0
        print('0')
    elif key == 49: #数字1
        print('1')
    elif key == 50: #数字2
        print('2')
    elif key == 51: #数字3
        print('3')
    elif key == 52: #数字4
        print('4')
    elif key == 53: #数字5
        print('5')
    elif key == 54: #数字6
        print('6')
    elif key == 55: #数字7
        print('7')
    elif key == 56: #数字8
        print('8')
    elif key == 57: #数字9
        print('9')
    elif key == 46: #数字9
        print('.')
    elif key == 8: #回退键
        print('b')

def touchInit():
    cmdUtil.release(0,0)
    time.sleep(0.1)
    cmdUtil.release(540,960)
    time.sleep(0.1)
    cmdUtil.release(1080,1920)
    time.sleep(0.1)
    cmdUtil.release(540,960)
    time.sleep(0.1)
    cmdUtil.release(0,0)

def main():
    net_thread = serialUtil.serialThread(CMDQUEUE)
    net_thread.setDaemon(True)
    net_thread.start()
    time.sleep(3)
    cmdUtil.initCmdUtil(CMDQUEUE,IMGSIZE)
    # touchInit()
    time.sleep(0.12)
    img = cv2.imread('m.jpg')
    while True:
        cv2.imshow('fengmm521.taobao.com', img)
        key = cv2.waitKey(0)
        if key > 0:
            onKey_Event(key)

if __name__ == '__main__':  
    main()
