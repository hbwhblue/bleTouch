import pyautogui
 
# 寻找截图所在位置
location = pyautogui.locate('2.jpg','1.jpg',confidence=0.5)
#location = pyautogui.locateCenterOnScreen('2.jpg',confidence=0.8)
location1 = pyautogui.locateCenterOnScreen('3.jpg',confidence=0.8)
# 打印截图所在位置
# print('低精度匹配')
print(location)
# print('高精度匹配')
print(location1)

# ————————————————
# 版权声明：本文为CSDN博主「Qin.Lu」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
# 原文链接：https://blog.csdn.net/m0_53043024/article/details/133667872
