#!/usr/bin/env python
# -*- coding: utf-8 -*-
from paddleocr import PaddleOCR
import json
import time
 
# 很多文章没有 lang="ch" 后面的代码，导致不断报错，无法运行！
# https://mp.weixin.qq.com/s?__biz=MzU5NDM1MjU5Mg==&mid=2247485479&idx=1&sn=9de2d952b2336d10ceed2412f1857120&chksm=fe03c898c974418e964196ba237bbcc7917241858c040fea9e95304a13809d96433b08a5a0b0&scene=21#wechat_redirect
#CPU版
# ocr = PaddleOCR(use_angle_cls=True,use_gpu=False, lang="ch", det_model_dir="./inference/det/",rec_model_dir="./inference/rec/",cls_model_dir="./inference/cls/")    

#GPU版
# ocr = PaddleOCR(use_angle_cls=True,use_gpu=True, lang="ch", det_model_dir="./inference/det/",rec_model_dir="./inference/rec/",cls_model_dir="./inference/cls/")    

class OCRObj(object):
    """docstring for Stat"""
    def __init__(self,pUse_gpu = False,pDet_model_dir="./inference/det/",pRec_model_dir="./inference/rec/",pCls_model_dir="./inference/cls/"):
        self.isGPU = pUse_gpu
        self.detPth = pDet_model_dir
        self.recPth = pRec_model_dir
        self.clsPth = pCls_model_dir
        self.isOcring = False
        self.ocr = PaddleOCR(use_angle_cls=True,use_gpu=self.isGPU, lang="ch", det_model_dir=self.detPth,rec_model_dir=self.recPth,cls_model_dir=self.clsPth)    
    #使用cv图片进行识别
    def ocrImg(self,img):
        self.isOcring = True
        ts = time.time()
        result = self.ocr.ocr(img)
        te = time.time()
        dt = te-ts
        print('cast time:%.3f'%dt)
        self.isOcring = False
        return result
    #使用图片文件地址识别
    def ocrFile(self,fpth):
        self.isOcring = True
        result = self.ocr.ocr(fpth, cls=True)
        self.isOcring = False
        return result
    #是否正在识别图片
    def isOcr(self):
        return self.isOcring


# img_path = 'capimg.jpg'
# result = ocr.ocr(img_path, cls=True)
# out = ''
# for line in result:
#     print(line)
#     out += json.dumps(line,ensure_ascii=False)

# f = open('out.txt','w',encoding='utf-8')
# f.write(out)
# f.close()

def main():
    pass

if __name__ == '__main__':
    main()
